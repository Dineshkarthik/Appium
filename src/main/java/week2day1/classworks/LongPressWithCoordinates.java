package week2day1.classworks;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;


public class LongPressWithCoordinates {

	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		DesiredCapabilities dc=new DesiredCapabilities();
		dc.setCapability("appPackage", "com.the511plus.MultiTouchTester");
		dc.setCapability("appActivity", "com.the511plus.MultiTouchTester.MultiTouchTester");
		dc.setCapability("deviceName", "Samsung S8");
		dc.setCapability("platformName", "Android");
		dc.setCapability("noReset", true);
		AndroidDriver<WebElement> driver=new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		//WebElement eleMsg = driver.findElementByXPath("(//android.widget.TextView[@resource-id='com.samsung.android.messaging:id/list_avatar_name'])[1]");

		Dimension size = driver.manage().window().getSize();
		int width = size.getWidth();
		int height = size.getHeight();
		int x=(int)(width*0.5);
		int y=(int)(height*0.5);
		TouchAction<?> action = new TouchAction<>(driver)
				.longPress(LongPressOptions.longPressOptions()
				.withPosition(PointOption.point(x, y))
				.withDuration(Duration.ofSeconds(2)))
				
						//.withElement(ElementOption.element(eleMsg))
//						.withDuration(Duration.ofSeconds(2)))
				.release()
				.perform();
	}

}
