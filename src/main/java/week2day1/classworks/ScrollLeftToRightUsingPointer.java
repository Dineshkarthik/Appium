package week2day1.classworks;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;



import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;

public class ScrollLeftToRightUsingPointer {

	public static void main(String[] args) throws MalformedURLException {
		DesiredCapabilities dc=new DesiredCapabilities();
		dc.setCapability("appPackage", "com.the511plus.MultiTouchTester");
		dc.setCapability("appActivity", "com.the511plus.MultiTouchTester.MultiTouchTester");
		dc.setCapability("deviceName", "Samsung S8");
		dc.setCapability("automatioName", "UIautomator2");
		dc.setCapability("platformName", "Android");
		dc.setCapability("noReset", true);
		AndroidDriver<WebElement> driver=new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Dimension size = driver.manage().window().getSize();
		int width = size.getWidth();
		int height = size.getHeight();
		int startX=(int)(width*0.2);
		int startY=(int)(height*0.5);
		int endX=(int)(width*0.8);
		int endY=(int)(height*0.5);
		PointerInput finger=new PointerInput(PointerInput.Kind.TOUCH, "finger");
		Sequence dragDrop=new Sequence(finger, 1);
		dragDrop.addAction(finger.createPointerMove(
				Duration.ofMillis(0), 
				PointerInput.Origin.viewport(), 
				startX, startY));
		dragDrop.addAction(finger.createPointerDown(PointerInput.MouseButton.LEFT.asArg()));
		dragDrop.addAction(finger.createPointerMove(
				Duration.ofMillis(500), 
				PointerInput.Origin.viewport(), 
				endX, endY));
		dragDrop.addAction(finger.createPointerUp(PointerInput.MouseButton.LEFT.asArg()));
		driver.perform(Arrays.asList(dragDrop));
		}

}
