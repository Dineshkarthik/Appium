package week2day1.classworks;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class ZoomIn {

	public static void main(String[] args) throws MalformedURLException {
		DesiredCapabilities dc=new DesiredCapabilities();
		dc.setCapability("appPackage", "com.the511plus.MultiTouchTester");
		dc.setCapability("appActivity", "com.the511plus.MultiTouchTester.MultiTouchTester");
		dc.setCapability("deviceName", "Samsung S8");
		dc.setCapability("automatioName", "UIautomator2");
		dc.setCapability("platformName", "Android");
		dc.setCapability("noReset", true);
		AndroidDriver<WebElement> driver=new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Dimension size = driver.manage().window().getSize();
		int width = size.getWidth();
		int height = size.getHeight();
		int startX=(int)(width*0.5);
		int startY=(int)(height*0.5);
		System.out.println("width is :"+width +" Height is :" +height);
		int endX=(int)(width*0.9);
		int endY=(int)(height*0.1);
		int startX1=(int)(width*0.5);
		int startY1=(int)(height*0.5);
		int endX1=(int)(width*0.1);
		int endY1=(int)(height*0.9);
		PointerInput finger=new PointerInput(PointerInput.Kind.TOUCH, "finger");
		Sequence dragDrop=new Sequence(finger, 1);
		dragDrop.addAction(finger.createPointerMove(
				Duration.ofMillis(0), 
				PointerInput.Origin.viewport(), 
				startX, startY));
		dragDrop.addAction(finger.createPointerDown(PointerInput.MouseButton.LEFT.asArg()));
		dragDrop.addAction(finger.createPointerMove(
				Duration.ofMillis(500), 
				PointerInput.Origin.viewport(), 
				endX, endY));
		dragDrop.addAction(finger.createPointerUp(PointerInput.MouseButton.LEFT.asArg()));
		PointerInput finger2=new PointerInput(PointerInput.Kind.TOUCH, "finger2");
		Sequence dragDrop2=new Sequence(finger2, 1);
		dragDrop2.addAction(finger2.createPointerMove(
				Duration.ofMillis(0), 
				PointerInput.Origin.viewport(), 
				startX1, startY1));
		dragDrop2.addAction(finger2.createPointerDown(PointerInput.MouseButton.LEFT.asArg()));
		dragDrop2.addAction(finger2.createPointerMove(
				Duration.ofMillis(500), 
				PointerInput.Origin.viewport(), 
				endX1, endY1));
		dragDrop2.addAction(finger2.createPointerUp(PointerInput.MouseButton.LEFT.asArg()));
		driver.perform(Arrays.asList(dragDrop, dragDrop2));
		
	}

}
